package org.mbracero.controller;

import org.mbracero.integration.Greeter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class GreeterController extends BaseController {
	
	private static final String DEFAULT_MESSAGE = "DEFAULT MESSAGE";
	
	@Autowired /*@Qualifier("greeterService")*/ Greeter greeter;
	
	@RequestMapping(value="/greeter", method=RequestMethod.GET)
	public @ResponseBody void greetingMsg(@RequestParam(value="msg", required=false) String msg) {
		String message = (msg != null && !"".equals(msg) ? msg : DEFAULT_MESSAGE);
		this.greeter.sendGreeting("GET msg :: " + message);
	}
	
	@RequestMapping(value="/greeter/{word}", method=RequestMethod.GET)
	public @ResponseBody void greetingGetWord(@PathVariable("word") String word) {
		String message = (word != null && !"".equals(word) ? word : DEFAULT_MESSAGE);
		this.greeter.sendGreeting( "GET word :: " + message);
	}
	
	@RequestMapping(value="/greeter", method=RequestMethod.POST)
	public @ResponseBody void greetingPostMsg(@RequestParam(value="msg", required=false) String msg) {
		String message = (msg != null && !"".equals(msg) ? msg : DEFAULT_MESSAGE);
		this.greeter.sendGreeting( "POST msg :: " + message);
	}
}
