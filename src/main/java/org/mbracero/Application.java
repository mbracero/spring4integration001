package org.mbracero;

import org.mbracero.component.CORSFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;

//This annotation tells Spring to auto-wire your application
@EnableAutoConfiguration
//This annotation tells Spring to look for controllers, etc.
@ComponentScan(value={"org.mbracero.controller", "org.mbracero.integration"})
//This annotation tells Spring that this class contains configuration
//information
//for the application.
@Configuration
@EnableWebSecurity
public class Application extends WebSecurityConfigurerAdapter {
	public static void main(String[] args) {
		// This call tells spring to launch the application and
		// use the configuration specified in LocalApplication to
		// configure the application's components.
		SpringApplication.run(Application.class, args);
	}
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
		http
		.csrf().disable() /**
							POST http://localhost:9000/api/items 403 (Forbidden)
							{"timestamp":1422297664419,"error":"Forbidden","status":403,"message":"Expected CSRF token not found. Has your session expired?"}
							
							CSRF protection is enabled in java configuration by default. You must disable it
						  **/
		.addFilterBefore(new CORSFilter(), ChannelProcessingFilter.class) /**
																			Enable CORS
																		  **/
		;
    }
}