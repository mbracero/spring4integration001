package org.mbracero.integration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component//("greeterService")
public class GreeterImpl implements Greeter {

	private final Logger log = Logger.getLogger(this.getClass()); // log4j
	
	protected final Log logger = LogFactory.getLog(this.getClass());
	
	@Override
	public void sendGreeting(String msg) {
		log.debug("LOG - Debug :: " + msg);
		log.info("LOG - Info :: " + msg);
		log.warn("LOG - Warn :: " + msg);
		log.error("LOG - Error :: " + msg);
		
		logger.debug("LOGGER - Debug :: " + msg);
		logger.info("LOGGER - Info :: " + msg);
		logger.warn("LOGGER - Warn :: " + msg);
		logger.error("LOGGER - Error :: " + msg);
	}

}
