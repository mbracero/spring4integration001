package org.mbracero.component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

@Component
public class CORSFilter implements Filter {
	
	private static final String HEADER_ORIGIN = "Origin";
	
	@SuppressWarnings("serial")
	private final List<String> validOrigins = new ArrayList<String>(){{
        add("http://localhost");
        add("http://127.0.0.1");
    }};

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		
		String originRequest = request.getHeader(HEADER_ORIGIN);
		if (validOrigins.contains(originRequest)) {		
			response.setHeader("Access-Control-Allow-Origin", originRequest);
			response.setHeader("Access-Control-Allow-Methods", "GET, OPTIONS");
			response.setHeader("Access-Control-Allow-Headers", "content-type");
		}
		
		chain.doFilter(req, res);
	}

	@Override
	public void destroy() {}

}
