package spring4integration001;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mbracero.integration.Greeter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/META-INF/spring/integration/hello_world.xml"})
public class HelloWorldTest { 
	@Autowired
	@Qualifier("messageChannel")
	MessageChannel messageChannel;
	
	@Autowired Greeter greeter;
	
	@Test
	public void testHelloWorld() {
		Message<String> helloWorld = new GenericMessage<String>("******************************* Hello World *******************************");
		messageChannel.send(helloWorld);
		assertEquals("Ok", "Ok");
	}
	
	@Test
	public void testHelloWorldByChannel() {
		this.greeter.sendGreeting(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Hello World <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		assertEquals("Ok", "Ok");
	}
	
	@Test
	public void testHelloWorldByChannel2() {
		System.out.println("--------------------------------------------------------- Started..");
		
		long start = System.nanoTime();
		for (int i=0;i<10;i++){
			this.greeter.sendGreeting(String.format("--------------------------------------------------------- Hello World %d",i));
		}
		System.out.println("--------------------------------------------------------- Completed..");
		System.out.println(String.format("--------------------------------------------------------- Took %f ms", (System.nanoTime()-start)/10e6));
		assertEquals("Ok", "Ok");
	}
}