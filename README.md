## Spring4Integration001

**Spring4 (boot, mvc) + gradle + CORS + INTEGRATION + log4j**

Simple prueba de spring integration

Log mediante log4j (ver log4j.xml)

Generate java structure command:
`gradle init --type java-library`


**CURL**

`curl -v -X GET http://localhost:9000/api/greeter/Palabra`

`curl -v -X GET http://localhost:9000/api/greeter?msg=Mensaje`

`curl -v -X POST -d 'msg=Este es el mensaje' http://localhost:9000/api/greeter`


Existen TEST JUnit para pruebas.



